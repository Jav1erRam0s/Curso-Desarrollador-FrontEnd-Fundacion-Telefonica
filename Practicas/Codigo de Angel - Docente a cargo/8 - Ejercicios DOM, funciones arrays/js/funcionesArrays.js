
var ciudades = ['Buenos Aires', 'Madrid', 'Barcelona', 'Bilbao', 'Valladolid'];


document.getElementById('muestraInfo').innerHTML ='El valor de la variable es ' + ciudades + '<br>';
console.log(ciudades.length);

// pop, nos elimina la última posición del array. 
// push, añade elemento al final del array. equivale a ciudades.length

ciudades.pop();

document.getElementById('muestraInfo').innerHTML +='El valor después de hacer pop  ' + ciudades + '<br>';

ciudades.push('Sevilla');
document.getElementById('muestraInfo').innerHTML +='El valor con push es  ' + ciudades + '<br>';


// shift, elimina primera posición de array. 
// unshif, añade al principio de nuesro array. 

ciudades.shift();
document.getElementById('muestraInfo').innerHTML +='El valor con shift es  ' + ciudades + '<br>';

ciudades.unshift('Málaga');
document.getElementById('muestraInfo').innerHTML +='El valor con unshift es  ' + ciudades + '<br>';

// splice, primer elemento indica la posición donde añade elemento o desde se borra, los elementos que quiere borrar, tercero elemento a añadir 

ciudades.splice(1,ciudades.length-1, 'Toledo');
document.getElementById('muestraInfo').innerHTML +='El valor con splice es  ' + ciudades + '<br>';

console.log(ciudades.length);

var ciudades2 = ['París', 'Roma', 'San Juan'];

// concat, une dos arrays generando uno. 


var ciudadesTotal = ciudades2.concat(ciudades);
// document.getElementById('muestraInfo').innerHTML +='El valor con concat es  ' + ciudadesTotal + '<br>';

document.querySelector('#muestraInfo').innerHTML +='El valor con concat es  ' + ciudadesTotal + '<br><hr>';
/*

JAVASCRIPT 
document.getElementById('muestraInfo')
document.getElementsByClassName('muestraInfo')
document.getElementByTagName('muestraInfo')

JQUERY 
$('#muestraInfo') , para acceder a id
$('.clase'), para acceder a clase
$('p'), para acceder a etiqueta html

*/



// for y while ESTRUCTURA REPETITIVA O BUCLE. 
document.querySelector('#muestraInfo').innerHTML += 'antes de for <br>';

for(var i=0; i<ciudadesTotal.length;i++){
	
	document.querySelector('#muestraInfo').innerHTML += 'En la posición ' + (i+1) + ' está ubicada la ciudad ' + ciudadesTotal[i] + '<br>';
}

i=0;

document.querySelector('#muestraInfo').innerHTML += ' <br><hr>antes de while<br>';

while(i<ciudadesTotal.length){
	
	document.querySelector('#muestraInfo').innerHTML += 'En la posición ' + (i+1) + ' está ubicada la ciudad ' + ciudadesTotal[i] + '<br>';
	i++;
}


















