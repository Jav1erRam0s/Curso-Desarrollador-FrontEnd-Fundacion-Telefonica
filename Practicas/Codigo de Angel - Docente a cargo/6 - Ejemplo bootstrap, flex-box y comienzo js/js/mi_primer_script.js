		/* Funciones pre definidas de javascript para mostrar información al usuario o
		para realizar debug, trazabilidad*/
		// alert('Mi primera ventana emergente de javascript');
		console.log('mensaje que se muestra en el inspector');
		// palabra reservada para devolver info, return 

		// definición de variables en javascript string y number. 
		var nombre = prompt('Por favor inserte su nombre');
		var edad = 35;
		// las dos formas que tenemos de generar arrays. 
		var ciudades = ['Buenos Aires', 'MAdrid', 'Barcelona'];
		var colores = new Array('Rojo', 'Amarillo','Negro');
		// variable tipo boolean valor true o false. 
		var accede = false;
		// variable tipo object. 
		var empleado = {nombre: 'Juan', edad: 35, departamento: 'ventas', vehiculo: false };

		let valor = 7;
		const apellido = 'Cruzado';
		console.log('El nombre es ' + nombre + ' y su edad ' + edad);

		// cambio de valor a variables ya creadas. 
		edad = 42;
		valor = 'planta 7';
		
		console.log(nombre);
		console.log('El nombre del empleado es '+ empleado.nombre);
		console.log('El valor de edad es '+ edad);
		console.log('El valor de numero es '+ valor);
		console.log(colores.length);
		
		document.write( '<p> El nombre del usuario es <b> ' + nombre + ' </b></p>');

		for(let i=0;i<colores.length;i++){
			console.log('En la posición ' + (i+1)+ ' se encuentra el color ' + colores[i]);
		}

		for(let i=0;i<ciudades.length;i++){
			document.write('<p>En la posición ' + (i+1)+ ' se encuentra el color ' + ciudades[i] +  ' </p>');
		}
					
		for(let i=100;i<500;i+=100){
			console.log('El puntero vale '+ i + ' y su división es de ' + i/20);
		}

		document.write( '<p> y su edad es <b> ' + edad + ' </b></p>');

		var i=100;

		console.log('Devuelve ' + isNaN(i)); 
		console.log('Devuelve ' + isNaN(nombre));

		while(i<500){
			console.log('El puntero vale '+ i + ' y su división es de ' + i/20);	
			i+=100;
		}

	