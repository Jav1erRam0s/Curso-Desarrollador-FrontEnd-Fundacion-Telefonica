
/* como se accede a los distintos elementos de html

- Mediante etiqueta html
document.getElementsByTagName()
- Mediante atributo class
document.getElementsByClassName()
- Mediante atributo id
document.getElementById()
*/

//  formas de acceso a los nodos de html ya creados. 
var parrafo = document.getElementsByTagName('p');
var titulo = document.getElementsByTagName('h1');

console.log(parrafo[1]);
console.log(titulo[0]);

var pclase = document.getElementsByClassName('etiquetap');
console.log(pclase);
console.log(pclase[0]);

var pid = document.getElementById('pid');
console.log(pid);

// CREACIÓN DE NODOS. 

var h1 = document.createElement('h1');
var contenido = document.createTextNode('Encabezado de mi software');

var h5 = document.createElement('h5');
var contenidoh5 = document.createTextNode('fin de página');

h1.appendChild(contenido);

h5.appendChild(contenidoh5);

// document.getElementsByTagName('header')[0].appendChild(h1);

document.body.appendChild(h5);


// MODIFICACIÓN DE UN ELEMENTO O NODO YA CREADO. 


var elem = document.getElementById('pid')
var elem2 = document.getElementById('pid2');

elem2.textContent += ' Incluyo nuevo texto mediante  <b>text content</b>'
elem.innerHTML += ' Incluyo nuevo texto mediante <b>inner HTML</b>';


// ELIMINAR NODO Y PONER OTRO NUEVO. 

// replaceChild(nodoNuevo, nodoQueQuieroEliminar)
// replaceChild(document.createElement('h3'),document.getElementsByTagName('h1')[0])
// document.getElementById() == $('#id')

var header = document.getElementsByTagName('header')[0];

var h1 = document.getElementById('tituloEncabezado');

var h3 = document.createElement('h3');
var contenidoh3 = document.createTextNode('nuevo encabezado');
h3.appendChild(contenidoh3);

header.replaceChild(h3,h1);


// ELIMINAR NODO 

var header = document.getElementById('encabezado');
var nodoAEliminar = document.getElementsByTagName('h3')[0];
header.removeChild(nodoAEliminar);

// ACCESO A ATRIBUTOS

// getAttribute setAttribute

var clase = document.getElementById('encabezado').getAttribute('class');

document.getElementById('encabezado').setAttribute('class', 'segundaClase');
// document.getElementById('titulo').setAttribute('class', 'tituloJS')
var hipervinculo = document.getElementById('enlaceExterno').getAttribute('href');

document.getElementById('enlaceExterno').href = 'https://conectaempleo-formacion.fundaciontelefonica.com'
document.getElementById('enlaceExterno').style.color = 'red';
document.getElementById('enlaceExterno').style.textDecoration = 'none';
document.getElementById('enlaceExterno').style.margin = '10px';


// evento click provoca que se ejecute función de cambio estilo. 

/*FUNCIONES NO ANÓNIMAS

function cambioEstilos (){
	// cambio de estilos que el diseñador quiera implementar
}

llamar a la función que he creado anteriormente
cambioEstilos();

$('id').click(cambioEstilos);

FUNCIÓN ANÓNIMA
$('ID').click(function (){
	// cambio de estilos que el diseñador quiera implementar. 
})

operador flecha, surge en EC6
=> 

$('#boton')click(function(){
	$('enlaceExterno').css({color: red, text-decoration: none, margin: 10px})
	})
 fadeIn fadeOut
 text-decoration: none textDecoration
*/

var evento1 = document.getElementById('primer_evento');

evento1.addEventListener('click', function(){
	document.getElementById('ultimap').style.fontSize = '30px';
	document.getElementById('ultimap').style.fontFamily = 'tahoma';

	
});

var h1_pulsado = document.getElementById('titulo');

h1_pulsado.addEventListener('click', function(){
	document.getElementById('pid2').setAttribute('class', 'modifico_estilo');
});

h1_pulsado.onmouseover = cambioColorH1;

function cambioColorH1(){
	document.getElementById('pid2').style.color = 'pink';
}

var a = document.getElementById('enlaceExterno');

a.onmouseover = detectoEvento;

function detectoEvento(){
	document.getElementById('enlaceExterno').style.color = 'green';
}

document.getElementById('enlaceExterno').onmouseout = cambioColor;

function cambioColor(){
	document.getElementById('enlaceExterno').style.color = 'blue';
}


// Resolución ejercicio 1

var span = document.getElementsByTagName('span')[0];


span.addEventListener('mouseover', function (){
	span.setAttribute('id', 'estilosNuevos');
	document.getElementById('estilosNuevos').onclick = estiloNuevo;
})


function estiloNuevo(){
	document.getElementById('estilosNuevos').style.color = 'gray';
	document.getElementById('estilosNuevos').style.fontSize = '40px';
	

}







































