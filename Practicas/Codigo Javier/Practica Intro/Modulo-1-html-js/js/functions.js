
function suma()
{
    var valor1 = document.getElementById('entrada1').value;
    var valor2 = document.getElementById('entrada2').value;
    var miSuma;
    miSuma = parseInt(valor1)+parseInt(valor2);
    document.getElementById('respuestaSuma').innerHTML=' La suma es: '+miSuma;
}

function factorial()
{
    var valor = parseInt(document.getElementById('entrada3').value);
    var miFactorial = 1;
    var i;
    for (i = 1; i <= valor; i++) 
    {
        miFactorial = miFactorial*i;
    } 
    document.getElementById('respuestaFactorial').innerHTML=' El factorial es : '+miFactorial;
}

function sumaEsPrimo()
{
    var valor1 = parseInt(document.getElementById('entrada1').value);
    var valor2 = parseInt(document.getElementById('entrada2').value);
    var suma = valor1 + valor2;
    var respuesta = true;
    for (var i=suma-1; i>1; i--) 
    {
        if(suma%i===0)
        {
            respuesta = false;
            break;
        }
    } 
    if(respuesta)
    {
        document.getElementById('respuestaSumaEsPrimo').innerHTML=' La suma es primo: TRUE';
    }
    else
    {
        document.getElementById('respuestaSumaEsPrimo').innerHTML=' La suma es primo: FALSE';
    }
}