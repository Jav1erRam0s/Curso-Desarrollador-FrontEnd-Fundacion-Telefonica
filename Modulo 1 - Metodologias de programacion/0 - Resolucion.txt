
--------------------------------------------------------------------------------------------------------------------------

ARQUIETCTURA VON NEWMAN

La arquietectura de von newman esta compuesta por 4 partes:
-Memoria (contiene datos y programas)
-CPU
-Unidad de entradas y salidas (I/O)
-Sistemas de interconexion

La arquitectura Von Neumann se basa en tres propiedades:
1) Hay un �nico espacio de memoria de lectura y escritura, que contiene las instrucciones y los datos necesarios.
2) El contenido de la memoria es accesible por posici�n, independientemente de que se acceda a datos o a instrucciones.
3) La ejecuci�n de las instrucciones se produce de manera secuencial: despu�s de ejecutar una instrucci�n se ejecuta la 
   instrucci�n siguiente que hay en la memoria principal, pero se puede romper la secuencia de ejecuci�n utilizando 
   instrucciones de ruptura de secuencia.

El sistema de interconexion es un medio de comunicaci�n compartido o multipunto donde se conectan todos los componentes que 
se quiere interconectar. Como se trata de un medio compartido, es necesario un mecanismo de control y acceso al bus. 
El sistema de interconexi�n es necesario pero generalmente no se considera una unidad funcional del computador.

--------------------------------------------------------------------------------------------------------------------------

ARQUIETCTURA HARDVARD

La organizaci�n del computador seg�n el modelo Harvard, b�sicamente, se distingue del modelo Von Neumann por la divisi�n 
de la memoria en una memoria de instrucciones y una memoria de datos, de manera que el procesador puede acceder separada 
y simult�neamente a las dos memorias.

El procesador dispone de un sistema de conexi�n independiente para acceder a la memoria de instrucciones y a la memoria de
datos. Cada memoria y cada conexi�n pueden tener caracter�sticas diferentes; por ejemplo, el tama�o de las palabras de 
memoria (el n�mero de bits de una palabra), el tama�o de cada memoria y la tecnolog�a utilizada para implementarlas. 

--------------------------------------------------------------------------------------------------------------------------

Comparacion

Von newman: La lectura, instruccion y escritura no pueden hacerse simultaneamente.
Hardvard: La instruccion y el acceso a los datos de memoria se pueden hacer simultaneamente, sin acceso a cache.

--------------------------------------------------------------------------------------------------------------------------
